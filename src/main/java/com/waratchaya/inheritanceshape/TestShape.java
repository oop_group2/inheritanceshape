/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.inheritanceshape;

/**
 *
 * @author Melon
 */
public class TestShape {
    public static void main(String[] aregs) {
        Circle circle1 = new Circle(2);
        Circle circle2 = new Circle(17);
        Triangle triangle1 = new Triangle(7,2);
        Triangle triangle2 = new Triangle(17,22);
        Rectangle rectangle1 = new Rectangle(8,5);
        Rectangle rectangle2 = new Rectangle(9,2);
        Square square1 = new Square(5);
        Square square2 = new Square(7);
        
        Shape[] shapes = {circle1, circle2, triangle1, triangle2, rectangle1, rectangle2, square1, square2};
        for(int i=0; i<shapes.length; i++) {
            shapes[i].Show();
        }
    }
}
