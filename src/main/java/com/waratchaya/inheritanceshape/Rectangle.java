/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.inheritanceshape;

/**
 *
 * @author Melon
 */
public class Rectangle extends Shape{
    protected double width;
    protected double height;
    
    public Rectangle(double width, double height) {
        if(width == 0) {
            System.out.println("Error : Width  equal Zero!!!");
            return;
        }
        if(height == 0) {
            System.out.println("Error : Height  equal Zero!!!");
            return;
        }
        this.height = height;
        this.width = width;
    }

    @Override
    public double calArea() {
        return this.width*this.height;
    }
    
    @Override
    public void Show() {
        System.out.println("Rectangle Width: " + this.width + " Height is: " + this.height + " Area is: " + this.calArea());
    }
}
